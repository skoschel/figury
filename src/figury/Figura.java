/**
 * 
 */
package figury;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;
import java.util.Random;
import javax.swing.*;


/**
 * @author tb
 *
 */
public abstract class Figura extends JPanel implements Runnable, ActionListener {
	
	//
	static boolean stopped = false;
	//boolean catched = false; //klikniety ksztalt
	
	// wspolny bufor
	protected static Graphics2D buffer;
	protected Area area;
	// do wykreslania
	protected Shape shape;
	// przeksztalcenie obiektu
	protected AffineTransform aft;

	// przesuniecie
	private int dx, dy;
	// rozciaganie
	private double sf;
	// kat obrotu
	private double an;
	private int delay;
	protected static int width;
	protected static int height;
	private Color clr;

	protected static final Random rand = new Random();

	public Figura(Graphics2D buf, int del, int w, int h) {
		delay = del;
		buffer = buf;
		width = w;
		height = h;
		
		dx = 1 + rand.nextInt(5);
		dy = 1 + rand.nextInt(5);
		sf = 1 + 0.05 * rand.nextDouble();
		an = 0.1 * rand.nextDouble();

		clr = new Color(rand.nextInt(255), rand.nextInt(255), rand.nextInt(255), rand.nextInt(255));
		// reszta musi być zawarta w realizacji klasy Figure
		// (tworzenie figury i przygotowanie transformacji)

	}

	@Override
	public void run() {
		// przesuniecie na srodek
		aft.translate(width-(width*2/3), height-(height*2/3));
		area.transform(aft);
		shape = area;

		while (true) {
			// przygotowanie nastepnego kadru
			shape = nextFrame();
			try {
				Thread.sleep(delay);
			} catch (InterruptedException e) {
			}
		}
	}

	public static void reShape(int w, int h, Graphics2D buf, boolean cl) {
		width = w;
		height = h;
		buffer = buf;	
		stopped = cl;
	}
	
	protected Shape nextFrame() {
		// zapamietanie na zmiennej tymczasowej
		// aby nie przeszkadzalo w wykreslaniu
		area = new Area(area);
		aft = new AffineTransform();
		Rectangle bounds = area.getBounds();
		int cx = bounds.x + bounds.width / 2;
		int cy = bounds.y + bounds.height / 2;
		
		if(!stopped) {	
		
			// zwiekszenie lub zmniejszenie
			if (bounds.height >= 60 || bounds.height <= 10)
				sf = 1 / sf;

			// odbicie -- poprawione

			if ((cx - (bounds.width/2)) < 0 && dx < 0)
				dx = -dx;
			if((cx + (bounds.width/2)) > width && dx > 0)
				dx = -dx;
			if ((cy - (bounds.height/2)) < 0 && dy < 0)
				dy = -dy;
			if((cy + (bounds.height/2)) > height && dy > 0)
				dy = -dy;
		
			// konstrukcja przeksztalcenia
			aft.translate(cx, cy);
			aft.scale(sf, sf);
			aft.rotate(an);
			aft.translate(-cx, -cy);
			aft.translate(dx, dy);
			// przeksztalcenie obiektu
			area.transform(aft);
		}
		
		return area;
	}

	@Override
	public void actionPerformed(ActionEvent evt) {
		// wypelnienie obiektu
		//buffer.setColor(clr.brighter());
		buffer.setColor(clr);
		buffer.fill(shape);
		// wykreslenie ramki
		buffer.setColor(clr.darker());
		buffer.draw(shape);
	}
	
}

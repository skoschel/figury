package figury;

import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;
import java.awt.geom.Rectangle2D;
import java.awt.geom.*;

public class Gwiazda extends Figura {
	
	double x1, x2, x3, x4, x5, x6, y1, y2, y3, y4, y5, y6;
	
	public Gwiazda(Graphics2D buf, int del) {
		super(buffer, del, width, height);
		
		GeneralPath Gwiazda = new GeneralPath();
		
		double x[] = {0, 6, 6, 0, -6, -6};
		double y[] = {8, 4, -4, -8, -4, 4};
		
		Gwiazda.moveTo(x[0], y[0]);
		Gwiazda.lineTo(x[2], y[2]);
		Gwiazda.lineTo(x[4], y[4]);
		Gwiazda.lineTo(x[0], y[0]);
		
		Gwiazda.moveTo(x[1], y[1]);
		Gwiazda.lineTo(x[3], y[3]);
		Gwiazda.lineTo(x[5], y[5]);
//		Gwiazda.lineTo(x[1], y[1]);
		
		Gwiazda.closePath();
		
		shape = Gwiazda;
        aft = new AffineTransform();                                  
        area = new Area(shape);
        
	}  
		

}

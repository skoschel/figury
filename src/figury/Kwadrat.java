package figury;

import java.util.Random;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.geom.*;

public class Kwadrat extends Figura {

	public Kwadrat(Graphics2D buf, int del) {
		super(buffer, del, width, height);
		
		shape = new Rectangle2D.Float(0, 0, 10, 10);
        aft = new AffineTransform();                                  
        area = new Area(shape);
	}
	

	
}

package figury;

import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;
import java.awt.geom.GeneralPath;

public class Trojkat extends Figura {

	public Trojkat(Graphics2D buf, int del) {
		super(buffer , del, width, height);
		
		int y1Points[] = {30, 0, 30};
		int x1Points[] = {0, 15, 30};

		GeneralPath polygon = 
		        new GeneralPath(GeneralPath.WIND_EVEN_ODD,
		                        x1Points.length);
		polygon.moveTo(x1Points[0], y1Points[0]);

		for (int index = 1; index < x1Points.length; index++) {
		        polygon.lineTo(x1Points[index], y1Points[index]);
		};

		polygon.closePath();

		shape = polygon;
        aft = new AffineTransform();                                  
        area = new Area(shape);
	}

}

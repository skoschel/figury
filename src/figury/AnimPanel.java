package figury;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JPanel;
import javax.swing.Timer;

public class AnimPanel extends JPanel implements ActionListener{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	// bufor
	static Image image;
	// wykreslacz ekranowy
	static Graphics2D device;
	// wykreslacz bufora
	static Graphics2D buffer;

	private int delay = 30;

	private Timer timer;

	private static int numer = 0;

	public AnimPanel() {
		super();
		setBackground(Color.WHITE);
		timer = new Timer(delay, this);
	}

	public void initialize() {
		int width = getWidth();
		int height = getHeight();

		
		image = createImage(width, height);
		buffer = (Graphics2D) image.getGraphics();
		buffer.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		device = (Graphics2D) getGraphics();
		device.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		
		//ustawienie bialego tla
		buffer.setBackground(Color.WHITE);
	}
	
	
	public void reinitialize() {
	
		
		image = createImage(getWidth(), getHeight());
		buffer = (Graphics2D) image.getGraphics();
		buffer.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		device = (Graphics2D) getGraphics();
		device.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		buffer.setBackground(Color.WHITE);
		
		Figura.reShape(getWidth(), getHeight(), buffer, false); 
		
	}
	
	
	void addFig() {
		
		//kolejna fiugra
		Figura fig;
		if(numer == 0)
			fig = new Kwadrat(buffer, delay);
		else if(numer == 1)
			fig = new Elipsa(buffer, delay);
		else if(numer == 2)
			fig = new Trojkat(buffer, delay);
		else
		{
			fig = new Gwiazda(buffer, delay);
			numer = 0;
		}
		
		++numer;
		timer.addActionListener(fig);
		new Thread(fig).start();
	}
	


	void animate() {
		if (timer.isRunning()) {
			timer.stop();
			//zatrzymanie tworzenia klatek
			Figura.stopped = true;
		} else {
			timer.start();
			Figura.stopped = false;
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		device.drawImage(image, 0, 0, null);
		buffer.clearRect(0, 0, getWidth(), getHeight());
	}
	
	
	

	
}
